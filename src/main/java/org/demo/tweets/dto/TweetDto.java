package org.demo.tweets.dto;
import com.fasterxml.jackson.annotation.JsonInclude;
//import jdk.jshell.Snippet;
import lombok.*;
import org.demo.tweets.model.Tweet;
import org.demo.tweets.model.User;
import org.springframework.data.annotation.Transient;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL) //on gagne qq octets en ne prennant pas les trucs null
public class TweetDto {
    private String id;
    @NotBlank(message = "text must not be blank")
    @Size(max = 256, message = "tweet is max 256 characters")
    private String text;
    @NotNull
    private Source source;
    @Transient
    private String etag;
    UserDto user;
    LocalDateTime createDateTime;
    LocalDateTime updateDateTime;

    public Tweet toEntity(){
        return Tweet.builder()
                .id(id)
                .text(text)
                .source(source)
                .user(user.toEntity())
                .build();
    }

}
