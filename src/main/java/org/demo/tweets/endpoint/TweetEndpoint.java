package org.demo.tweets.endpoint;


import lombok.extern.slf4j.Slf4j;
import org.demo.tweets.dto.TweetDto;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.UriComponents;
import org.springframework.web.util.UriComponentsBuilder;

import javax.validation.Valid;
import java.util.UUID;

@RestController
@RequestMapping(value="api/v1/tweets")
@CrossOrigin(origins = "*")
@Slf4j
public class TweetEndpoint {

    @PostMapping
    @ResponseBody
    public ResponseEntity<TweetDto>
    createTweet(@Valid @RequestBody TweetDto tweetDto,
                UriComponentsBuilder uriComponentsBuilder) {
        log.debug(tweetDto.toString());
        tweetDto.setId(UUID.randomUUID().toString());
        UriComponents uriComponents = uriComponentsBuilder.path("/api/v1/tweets/{id}")
                .buildAndExpand(tweetDto.getId());
        return ResponseEntity.status(HttpStatus.CREATED)
                .location(uriComponents.toUri())
                .body(tweetDto);
    }
}
