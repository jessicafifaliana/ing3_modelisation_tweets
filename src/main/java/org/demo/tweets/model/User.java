package org.demo.tweets.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.demo.tweets.dto.UserDto;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Document
public class User {
    @NotBlank
    private String nickname;
    @Email
    private String mail;

    public UserDto toDto(){
        return UserDto.builder()
                .mail(mail)
                .nickname(nickname)
                .build();
    }
}