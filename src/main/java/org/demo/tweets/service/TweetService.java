package org.demo.tweets.service;

import lombok.extern.slf4j.Slf4j;
import lombok.var;
import org.demo.tweets.model.Tweet;
import org.demo.tweets.repository.TweetRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
@Slf4j
@Service
public class TweetService {
    @Autowired
    private TweetRepository tweetRepository;
    public Tweet createTweet(Tweet tweet){
        log.debug("tweedto {}",tweet.toString());
        var insertedTweet=tweetRepository.insert(tweet);
        return insertedTweet;
    }

}
