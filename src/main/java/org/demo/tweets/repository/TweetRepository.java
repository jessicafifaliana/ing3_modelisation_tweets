package org.demo.tweets.repository;

import org.demo.tweets.model.Tweet;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TweetRepository  extends MongoRepository<Tweet,String> {
}
